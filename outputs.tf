output "id" {
  description = "Integer that uniquely identifies the project within the gitlab install"
  value       = gitlab_project.main.id
}

output "path_with_namespace" {
  description = "The path of the repository with namespace"
  value       = gitlab_project.main.path_with_namespace
}

output "ssh_url_to_repo" {
  description = "URL that can be provided to git clone to clone the repository via SSH"
  value       = gitlab_project.main.ssh_url_to_repo
}

output "http_url_to_repo" {
  description = "URL that can be provided to git clone to clone the repository via HTTP"
  value       = gitlab_project.main.http_url_to_repo
}

output "web_url" {
  description = "URL that can be used to find the project in a browser"
  value       = gitlab_project.main.web_url
}

output "runners_token" {
  description = "Registration token to use during runner setup"
  value       = gitlab_project.main.runners_token
}

//noinspection HILUnresolvedReference
output "branch_protection_ids" {
  description = "The IDs of the protected branches (not the branch name) for each protected branch"
  value       = { for name, protection in gitlab_branch_protection.main : name => protection.branch_protection_id }
}
