resource "gitlab_project" "main" {
  name         = var.name
  path         = var.path
  description  = var.description
  namespace_id = var.group_id

  lfs_enabled    = var.lfs_enabled
  default_branch = var.default_branch

  issues_enabled         = true
  merge_requests_enabled = var.merge_requests_enabled

  wiki_enabled     = false
  snippets_enabled = var.snippets_enabled

  packages_enabled           = var.packages_enabled
  container_registry_enabled = var.container_registry_enabled

  pipelines_enabled      = var.pipelines_enabled
  shared_runners_enabled = var.shared_runners_enabled

  # var.pages_enabled = false must have preference, even if an access level is set.
  # Treat var.pages_access_level as a setting which can be set always on the module,
  # independently from var.pages_enabled is set or not (default value applies).
  pages_access_level = var.pages_enabled ? (
    var.pages_access_level != null ? var.pages_access_level : "enabled"
  ) : "disabled"

  merge_method  = "merge"
  squash_option = "never"

  remove_source_branch_after_merge = true

  only_allow_merge_if_pipeline_succeeds = var.pipelines_enabled
  allow_merge_on_skipped_pipeline       = var.allow_merge_on_skipped_pipeline

  only_allow_merge_if_all_discussions_are_resolved = true

  visibility_level       = var.visibility_level
  request_access_enabled = false

  import_url                          = var.import_url
  mirror                              = var.import_url != null ? true : null
  mirror_trigger_builds               = var.import_url != null ? var.pull_mirror_trigger_builds : null
  mirror_overwrites_diverged_branches = var.import_url != null ? var.pull_mirror_overwrites_diverged_branches : null
  only_mirror_protected_branches      = var.import_url != null ? var.pull_mirror_only_protected_branches : null

  //noinspection HCLUnknownBlockType
  push_rules {
    member_check            = var.member_check
    commit_committer_check  = var.commit_committer_check
    reject_unsigned_commits = var.reject_unsigned_commits

    deny_delete_tag = true
    prevent_secrets = true

    max_file_size = 1

    author_email_regex = "@(${replace(join("|", var.author_domains), ".", "\\.")})$"

    branch_name_regex    = var.conventional_commits == true ? local.conventional_commits_branch_name_regex : ""
    commit_message_regex = var.conventional_commits == true ? local.conventional_commits_commit_message_regex : ""
  }

  tags = var.tags
}

//noinspection HILUnresolvedReference
resource "gitlab_branch_protection" "main" {
  project = gitlab_project.main.id
  branch  = each.key

  for_each = { for protected_branch in var.protected_branches
    : (protected_branch.branch == null ? var.default_branch : protected_branch.branch)
    => protected_branch
  }

  push_access_level  = each.value.push_access_level
  merge_access_level = each.value.merge_access_level

  code_owner_approval_required = each.value.code_owner_approval_required
}

resource "gitlab_tag_protection" "all" {
  project = gitlab_project.main.id
  tag     = "*"

  create_access_level = "no one"
}

resource "gitlab_tag_protection" "releases" {
  count = var.versionable ? 1 : 0

  project = gitlab_project.main.id
  tag     = "${local.version_prefix}${local.version_format}"

  create_access_level = "maintainer"
}

resource "gitlab_project_level_mr_approvals" "main" {
  project_id = gitlab_project.main.id

  reset_approvals_on_push                        = true
  merge_requests_author_approval                 = true
  merge_requests_disable_committers_approval     = false
  disable_overriding_approvers_per_merge_request = true
}

//noinspection HILUnresolvedReference
resource "gitlab_project_approval_rule" "main" {
  for_each = { for rule in var.approval_rules : rule.name => rule }
  project  = gitlab_project.main.id

  name      = each.value.name
  user_ids  = each.value.user_ids
  group_ids = each.value.group_ids

  approvals_required = each.value.approvals_required
}

resource "gitlab_project_badge" "pipeline_status_default" {
  count   = var.pipelines_enabled ? 1 : 0
  project = gitlab_project.main.id

  link_url  = "${gitlab_project.main.web_url}/-/commits/%%{default_branch}"
  image_url = "${gitlab_project.main.web_url}/badges/%%{default_branch}/pipeline.svg"
}

resource "gitlab_project_badge" "pipeline_status_extra" {
  project    = gitlab_project.main.id
  depends_on = [gitlab_project_badge.pipeline_status_default]

  //noinspection HILUnresolvedReference
  for_each = var.pipelines_enabled ? { for protected_branch in var.protected_branches
    // branch name => badge key width
    : protected_branch.branch => (80 + length(protected_branch.branch) * 5)
    if protected_branch.branch != null && protected_branch.branch != var.default_branch
  } : {}

  link_url  = "${gitlab_project.main.web_url}/-/commits/${each.key}"
  image_url = "${gitlab_project.main.web_url}/badges/${each.key}/pipeline.svg?key_text=pipeline@${each.key}&key_width=${each.value}&ignore_skipped=true"
}

resource "gitlab_project_badge" "conventional_commits" {
  count      = var.conventional_commits ? 1 : 0
  project    = gitlab_project.main.id
  depends_on = [gitlab_project_badge.pipeline_status_default, gitlab_project_badge.pipeline_status_extra]

  link_url  = "https://www.conventionalcommits.org/en/v1.0.0/"
  image_url = "https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow"
}

resource "gitlab_project_badge" "semver" {
  count      = local.semver ? 1 : 0
  project    = gitlab_project.main.id
  depends_on = [gitlab_project_badge.conventional_commits]

  link_url  = "https://semver.org/spec/v2.0.0.html"
  image_url = "https://img.shields.io/badge/Semamtic%20Versioning-2.0.0-orange"
}

resource "gitlab_project_badge" "changelog" {
  count      = local.changelog ? 1 : 0
  project    = gitlab_project.main.id
  depends_on = [gitlab_project_badge.semver]

  link_url  = "https://keepachangelog.com/en/1.1.0/"
  image_url = "https://img.shields.io/badge/Keep%20a%20Changelog-1.1.0-red"
}
