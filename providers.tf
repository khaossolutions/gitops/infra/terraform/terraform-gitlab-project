terraform {
  required_version = ">= 1.3.0"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.8.0, < 3.10.0"
    }
  }
}
