variable "name" {
  description = "The name of the project"
}

variable "path" {
  description = "The path of the repository"
}

variable "group_id" {
  description = "The group of the project"
}

variable "description" {
  description = "A description of the project"
  default     = ""
}

variable "lfs_enabled" {
  description = "Enable LFS for the project"
  default     = false
  type        = bool
}

variable "default_branch" {
  description = "The default branch for the project"
  default     = "latest"
}

variable "changelog" {
  description = "Does the project adhere to Keep a Changelog?"
  default     = null
  type        = bool
}

variable "versionable" {
  description = "Allow to create vX.Y.Z tags by maintainers"
  default     = false
  type        = bool
}

variable "version_prefix" {
  description = "Optional prefix for version tags created"
  default     = ""
}

variable "version_format" {
  description = "Format to match for version tags created"
  default     = "v{semver}"
}

locals {
  version_prefix = var.version_prefix != "" ? "${var.version_prefix}-" : ""
  version_format = replace(var.version_format, "{semver}", "*.*.*")

  semver    = var.versionable && length(regexall("{semver}", var.version_format)) > 0
  changelog = var.changelog == null ? local.semver : var.changelog
}

variable "merge_requests_enabled" {
  description = "Enable merge requests for the project"
  default     = true
  type        = bool
}

variable "snippets_enabled" {
  description = "Enable snippets for the project"
  default     = true
  type        = bool
}

variable "pipelines_enabled" {
  description = "Enable pipelines for the project"
  default     = true
  type        = bool
}

variable "shared_runners_enabled" {
  description = "Enable shared runners for this project"
  default     = true
  type        = bool
}

variable "container_registry_enabled" {
  description = "Enable container registry for the project"
  default     = false
  type        = bool
}

variable "packages_enabled" {
  description = "Enable packages repository for the project"
  default     = false
  type        = bool
}

variable "pages_enabled" {
  description = "Enable GitLab Pages for the project"
  default     = false
  type        = bool
}

variable "pages_access_level" {
  description = "Set the access level of GitLab Pages for the project. Defaults to the project's visibility level"
  default     = null
  type        = string

  validation {
    condition     = var.pages_access_level != null ? contains(["private", "public"], var.pages_access_level) : true
    error_message = "Available GitLab Pages access levels are \"private\" or \"public\"."
  }
}

variable "visibility_level" {
  description = "Set the visibility level of the project"
  default     = "private"
  type        = string

  validation {
    condition     = contains(["private", "internal", "public"], var.visibility_level)
    error_message = "Available visibility levels are \"private\", \"internal\" or \"public\" for self-hosted instances and \"private\" or \"public\" for gitlab.com projects."
  }
}

variable "allow_merge_on_skipped_pipeline" {
  description = "Treat skipped pipelines as if they finished with success."
  default     = false
  type        = bool
}

variable "import_url" {
  description = "Git URL to a repository to be imported"
  sensitive   = true
  default     = null
  type        = string
}

variable "pull_mirror_trigger_builds" {
  description = "Pull mirroring triggers builds"
  default     = false
  type        = bool
}

variable "pull_mirror_overwrites_diverged_branches" {
  description = "Pull mirror overwrites diverged branches"
  default     = true
  type        = bool
}

variable "pull_mirror_only_protected_branches" {
  description = "Only mirror protected branches"
  default     = true
  type        = bool
}

variable "author_domains" {
  description = "All commit author emails must match this email domains"
  type        = list(string)
}

variable "member_check" {
  description = "Restrict commits by author (email) to existing GitLab users"
  default     = true
  type        = bool
}

variable "commit_committer_check" {
  description = "Users can only push commits to this repository that were committed with one of their own verified emails"
  default     = true
  type        = bool
}

variable "reject_unsigned_commits" {
  description = "Reject commit when it’s not signed through GPG"
  default     = true
  type        = bool
}

variable "conventional_commits" {
  description = "Define if commit messages and branch names adhere to Conventional Commits"
  default     = true
  type        = bool
}

variable "push_rules_extra_branch_names" {
  description = "Extra branch names to allow outside of the conventional commits convention"
  default     = []
  type        = list(string)
}

locals {
  conventional_commits_branch_name_regex    = "(^(build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test)\\/[a-z0-9\\-]{1,55}$)|${join("|", concat([var.default_branch], var.push_rules_extra_branch_names))}"
  conventional_commits_commit_message_regex = "^((build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test)(\\(\\w+\\))?(!)?(: (.*\\s*)*))|(Merge (.*\\s*)*)"
}

variable "tags" {
  description = "Tags (topics) of the project"
  default     = []
  type        = list(string)
}

//noinspection TFIncorrectVariableType
variable "protected_branches" {
  description = "Configure the projects protected branches"
  default     = [{}]

  type = list(object({
    branch = optional(string, null)

    push_access_level  = optional(string, "no one")
    merge_access_level = optional(string, "maintainer")

    code_owner_approval_required = optional(bool, false)
  }))
}

variable "approval_rules" {
  description = "Manage approval rules of the project"
  default     = []

  type = list(object({
    name      = string
    user_ids  = optional(list(number), [])
    group_ids = optional(list(number), [])

    approvals_required = number
  }))
}
