# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.3.0] - 2023-02-04
### Fixed
- Migrate to Terraform v1.3 and use the introduced option object type attributes. (#34)

## [1.2.1] - 2022-02-10
### Fixed
- Allow gitlab provider versions v3.9.x to be used. No BC breaks in this provider release. (!41)

## [1.2.0] - 2021-11-29
### Added
- Allow `user_ids` in `approval_rules` for more flexible work-flows. (#26)
- With the provider update, `squash_option` is set to `never`. (#27)
- With the provider update, `allow_merge_on_skipped_pipeline` is set to `false`. (#27)
- Allow to configure `allow_merge_on_skipped_pipeline` setting. (#28)
- Add more outputs for the project and the branch protections. (#31)
- Configure GitLab Pages to enable them and set access level separately. (#32)

### Changed
- Require `~> 3.8.0` of the `gitlab` provider. (#27)

## [1.1.1] - 2021-11-23
### Fixed
- Require `~> 3.7.0` of the `gitlab` provider to only allow bug fix updates of it. New provider features may break 
  this module or introduce unintended changes not covered and configurable by this module. (#27)
- Disable "pipelines must succeed" setting if pipelines are disabled. (#29)
- Ensure correct ordering of project badges if no extra pipeline badges are created. (#30)
- Create extra pipeline badges only if pipelines are enabled. (#30)
- Add missing documentation about merge request configurations set by this module.

## [1.1.0] - 2021-10-24
### Added
- Add `visibility_level` parameter to configure the visibility of the project. (!28)

### Changed
- Publish the modules as Open Source project. (!28)

## [1.0.0] - 2021-10-15
### Added
- Add `version_format` parameter to configure the complete version format. Supports `{semver}` placeholder. (#22)
- Create GitLab project badges for protected branches pipelines, Conventional Commits, Semantic Versioning and
  Keep A Changelog. (#23)
- Allow disabling Conventional Commits enforcement on push rules for commit messages and branches. (#24)
- Allow enabling unverified authors/users and unsigned commits for push rules. (#17)
- Allow enabling GitLab container registry. (#18)
- Allow enabling GitLab package registry. (#20)
- Add [MIT license](LICENSE) in preparation to publish the module as Open Source project. (!27)

### Changed
- [BREAKING] Rename `versionable_prefix` to `version_prefix` for consistency. (#22)
- [BREAKING] Migrated to GitLab infrastructure registry. Use `gitlab.com/khaossolutions/project/gitlab` instead of
  `app.terraform.io/khaossolutions/project/gitlab` as module source for all versions. (#19)

### Fixed
- Extensively document all possibilities of this module including escape hatches if needed. (#21)

## [0.7.0] - 2021-09-27
### Added
- Add optional prefix to versionable tag protection. (#22)

### Changed
- Update gitlab provider to v3.7. (#16)

## [0.6.0] - 2021-05-18
### Added
- Protected branches are configurable now. (#14)
- Allow adding extra branch names to push rules. (#15)

## [0.5.1] - 2021-05-06
### Fixed
- Use `gitlab_project`'s default values on pull mirroring settings, when no import URL is defined (!13)

## [0.5.0] - 2021-05-01
### Added
- Configure pull mirroring on creating a project. (#12)

### Changed
- Author email domains for push rules are configurable. (#13)
- [BREAKING] Author email domains must be configured when using the module! (#13)
- Update gitlab provider to v3.6.

## [0.4.0] - 2021-04-09
### Changed
- Allow enabling Git LFS. (#9)
- Allow to disable pipelines. (#8)
- Allow disabling shared runners. (#11)
- Allow to disable snippets. (#10)
- Allow to disable Merge Requests. (#7)

### Fixed
- Create tag protection for release version tags only if requested. (!10)

## [0.3.0] - 2021-04-07
### Added
- Allow projects to be versionable with `vX.Y.Z` tags. (#6)

## [0.2.0] - 2021-04-06
### Added
- New setting to configure one or more approval rules for groups. (#5)

## [0.1.0] - 2021-04-06
### Added
- Create very opinionated GitLab project with issues, MRs, pipelines, shared runner and snippets enabled. (#1)
- Configure very strict push rules. (#1)
- Protect the default branch (`latest`) so only maintainers can merge, no one can push. (#1)
- Protect all tags, so no one can create tags. (#1)
- Configure permissive MR approval settings, so authors and committers are allowed to approve. Approvals are reset
  on push and can't be overridden in an MR. (#1)

[Unreleased]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v1.3.0...latest
[1.3.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v1.2.1...v1.3.0
[1.2.1]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v1.2.0...v1.2.1
[1.2.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.7.0...v1.0.0
[0.7.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.5.1...v0.6.0
[0.5.1]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/khaossolutions/gitops/infra/terraform/terraform-gitlab-project/-/compare/61437f21...v0.1.0
