# GitLab Project

This Terraform module wraps the creation of GitLab projects into an opinionated set of default settings.

This repo contains a [Terraform](https://www.terraform.io/) module to create a [Project](https://docs.gitlab.com/ee/user/project/)
on [GitLab](https://about.gitlab.com/). This module is designed to create the project with opinionated default settings.



## How do you use this module?

This repo defines a [Terraform module](https://www.terraform.io/docs/modules/usage.html), which you can use in your code
by adding a `module` configuration and setting its `source` parameter to module source of this project from the
[GitLab Terraform module registry](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/#reference-a-terraform-module).

```hcl
module "gitlab_project_example" {
  # Use version v1.3.* of the terraform-gitlab-project module
  source  = "gitlab.com/khaossolutions/project/gitlab"
  version = "~> 1.3.0"

  # Specify the name, path and group for the GitLab project
  name     = "Example project"
  path     = "example"
  group_id = gitlab_group.acme.id

  # Set topics on the GitLab project
  tags = ["Java", "OpenShift"]

  # ... See variables.tf for the other parameters you must define for the gitlab-project module
}
```

Note the following parameters:

- `source`: Use this parameter to specify the registry name of the gitlab-project module.
- `version`: Use this parameter to specify a specific version or version range for this module. That way, instead of
  using the latest version of this module, which will change every time you run Terraform, you're using a fixed version
  of the module.
- `name`: The name of the project shown in GitLab. It's not a problem to rename a project.
- `path`: That's the slug of the project which is used in building all URLs to the project.
- `group_id`: The ID of the GitLab group which should be the parent for the project. Accepts groups and subgroups.
- `tags`: Optional tags (topics) of the project.

You can find the other parameters in [variables.tf](variables.tf).



## What's included in this module?

This module consists of the following resources:

- [Project](#project)
- [Pull Mirroring](#pull-mirroring)
- [Push Rules](#push-rules)
- [Branch Protection](#branch-protection)
- [Tag Protection](#tag-protection)
- [Merge Requests](#merge-requests)
- [MR Approval Settings](#mr-approval-settings)
- [MR Approval Rules](#mr-approval-rules)
- [Badges](#badges)


### Project

This module creates a GitLab project with restrictive push rules, `latest` as default branch name and only issues, MRs,
pipelines and shared runners as well as snippets enabled by default. The following parameters can be modified:

- `default_branch`: Set the default branch of the project. This affects the default branch protection too. Default
  branch name is set to `latest`. Due to this, the default branch name setting of the group is ignored.
- `lfs_enabled`: Set to `true` to enable Git Large File System (LFS) for the project. Default is `false`
- `merge_requests_enabled`: Set to `false` to disable merge requests. Default is `true`
- `pipelines_enabled`: Set to `false` to disable GitLab CI/CD pipelines. Default is `true`
- `shared_runners_enabled`: Set to `false` to disable GitLab CI/CD shared runners. Default is `true`
- `container_registry_enabled`: Set to `true` to enable the GitLab container registry. Default is `false`
- `packages_enabled`: Set to `true` to enable the GitLab package & infrastructure registry. Default is `false`
- `snippets_enabled`: Set to `false` to disable project snippets. Default is `true`
- `pages_enabled`: Set to `true` to enable GitLab Pages. Default is `false`
- `pages_access_level`: Set to `public` or `private` to fine-tune the GitLab Pages access level if GitLab Pages are
  enabled. Defaults to the project's visibility if not set.
- `visibility_level`: Set to `public` to create a public project. Default is `private`, on self-hosted instances
  `internal` is available as an additional visibility level.

The following attributes are returned from this module for the project:

- `id`: Integer that uniquely identifies the project within the GitLab instance.
- `path_with_namespace`: The path of the repository with the full namespace.
- `ssh_url_to_repo`: URL that can be provided to git clone to clone the repository via SSH.
- `http_url_to_repo`: URL that can be provided to git clone to clone the repository via HTTP.
- `web_url`: URL that can be used to find the project in a browser.
- `runners_token`: Registration token to use during runner setup.
- `branch_protection_ids`: Map of the IDs of the protected branches for each protected branch with branch name as key.

#### Pull Mirroring

Configuration of pull mirroring via Terraform and this module is supported. Pull mirroring must be configured already
when creating the project. Changing the `import_url` requires recreation of the project. The default mirror
configuration pulls only protected branches and allows overwriting diverged changes in the mirror.

- `import_url`: The URL of the repository to mirror. Activates pull mirroring with the following parameters.
- `pull_mirror_trigger_builds`: Set to `true` to allow pull mirroring to trigger CI/CD pipelines. Default is `false`
- `pull_mirror_overwrites_diverged_branches`: Set to `false` to disable overwriting diverged branches. Default is `true`
- `pull_mirror_only_protected_branches`: Set to `false` to mirror all branches. Default is `true`

#### Push Rules

Push rules define a level of security and coding standards. By default, all commits must be GPG signed, can only be
committed by the user pushing the changes and the user must be a valid GitLab user, as well as the author email must
match the domains configured.

Additionally, the commit messages must follow the [Conventional Commits] guidelines by default. Branch names must be
prefixed with a conventional commit *type* too, e.g. `feat/new-exiting-stuff`, except the default branch.

The following parameters are configurable for push rules:

- `author_domains`: All commit authors emails must match this email domains. Required.
- `member_check`: Restrict commits by author (email) to existing GitLab users. Default is `true`
- `commit_committer_check`: Users can only push commits to this repository that were committed with one of their
  own verified emails. Default is `true`
- `reject_unsigned_commits`: Reject commit when it’s not signed through GPG. Default is `true`
- `conventional_commits`: Set to `false` to disable Conventional Commits push rules for commit messages and branch
  names. Default is `true`
- `push_rules_extra_branch_names`: List additional branch names which are allowed under Conventional Commits
  enforcement. Only useful if `conventional_commits` is set to `true`.


### Branch Protection

This module protects the default branch, so only Maintainers can merge MRs and no one is allowed to push by default. To
configure the branch protection and protect additional branches, configure the `protected_branches` list for each
branch.

This module uses the [`module_variable_optional_attrs` experiment](https://www.terraform.io/docs/language/expressions/type-constraints.html#experimental-optional-object-type-attributes)
to make certain parameters of the branch protection settings optional and set default values for them.

- `branch`: The branch name to protect. Omit to configure protection settings of the default branch.
- `push_access_level`: One of `no one`, `developer` or `maintainer` to allow pushes. Default is `no one`
- `merge_access_level`: One of `no one`, `developer` or `maintainer` to allow merging. Default is `maintainer`
- `code_owner_approval_required`: Set to `true` to require code owner approval before merging. Default is `false`

**Escape hatch:** Use the [`gitlab_branch_protection`] resource with the `id` attribute of this module as `project` to
configure branch protections individually on your own. Set `protected_branches` to an empty list (`[]`) to remove the
default branch protection (it's default value is set `[{}]` and filled in by the optional default values as mentioned
above).


### Tag Protection

This module protects all tags, so no one can create tags by default. To enable semantic versioning tags like
`vX.Y.Z` set `versionable` to `true`. Version tags are creatable by Maintainers only. The format of the version tags can be 
configured with the following parameters:

- `version_prefix`: Optional prefix to prepend to the version format, concatenated with a dash to it. For example set 
  it to e.g. `*`, so version tags like `module-vX.Y.Z` in monorepos can be created. Default is empty.
- `version_format`: Adapt the version format to your needs, e.g. if you don't add a `v` in front of the version 
  number set it to `{semver}` only. Default is `v{semver}`

**Note:** The `{semver}` string is a special placeholder which replaces the version format with the most specific 
placeholders allowed in the applicable GitLab version, to support [Semantic Versioning].

**Escape hatch:** Use the [`gitlab_tag_protection`] resource to configure any other tag protections with your own
matching tag patterns. Note, that the default tag protection of `no one` on all tags (`*`) can't be removed, but it's
overruled with more specific tag protection match patterns, like the releases tag protection works.


### Merge Requests

By default, merge requests are enabled to encourage a GitLab workflow, together with the restrictive branch protection
rules mentioned above.

To ensure consistent signed commits the merge method is set to merge commits and squash during the merge is disabled.
Squashing should be done by the developer to sign the squashed commits, and keep a reasonable commit history.

#### Merge Options

For a fast and clean GitLab workflow, merge request diff discussions are automatically resolved if they become
outdated and the "Delete source branch" option is enabled by default for new merge requests.

**Recommendation:** Enable merged results pipelines and configure pipelines for merge requests. Additionally enable
merge trains if multiple developers work on the project. Both settings are not yet configurable via the GitLab provider.

#### Merge Checks

By default, all discussions on the merge request must be resolved. Since merge request diff discussions are
automatically resolved if they become outdated, a fast GitLab workflow is still possible.

When pipelines are enabled (the default) the merge request pipelines must succeed to merge the merge request and 
skipped pipelines are considered harmful.

The following settings are configurable:

- `allow_merge_on_skipped_pipeline`: Set to `true` to treat skipped pipelines as if they finished with success. 
  Default is `false`

#### Merge suggestions

**Recommendation:** Set the commit message to the following message which is similar to the GitLab default, but
conforms to [Conventional Commits] when applying code suggestions. The setting is not yet configurable via the
GitLab provider.

```
refactor: apply %{suggestions_count} suggestion(s) to %{files_count} file(s)
```


### MR Approval Settings

This module contains more permissive MR approval settings. Although approvals are reset on every push, the author and
committers can approve MR if they are in the required approvals. In contrast to the other settings, it is more
permissive by default, as this module may be used by individual developers or groups without strict compliance settings
regarding who is allowed to approve changes.

Approvals can't be overridden in an MR, the project settings are the required settings for each MR.


### MR Approval Rules

This module allows configuring none, one or more approval rules for MR. Approval rules can be configured for groups
and/or users, and are required for all branches. Configure the `approval_rules` list for each approval rule with the
following parameters:

- `name`: The name of the approval rule.
- `user_ids`: A list of user IDs which can approve of the merge request.
- `group_ids`: A list of group IDs whose members can approve of the merge request.
- `approvals_required`: The number of approvals required for this rule.

**Escape hatch:** Use the [`gitlab_project_approval_rule`] resource with the `id` attribute of this module as `project`
to configure branch protections individually on your own.


### Badges

This module adds a few project badges. The badges and it's content are derived from the projects settings above.

- **Pipeline Status:** If CI/CD pipelines are enabled a pipeline status badge is created for all protected branches.
  Additionally, for all branches except the default branch, the pipeline status badge ignores skipped pipelines.
- **Conventional Commits:** A [Conventional Commits] badge is shown, if push rules force adhering to those
  conventions when `conventional_commits` is set to `true` (default).
- **SemVer:** A [Semantic Versioning] badge is shown, if the project is `versionable` and the `version_format`
  contains a `{semver}` placeholder.
- **Keep a Changelog:**: If the project adheres to [Keep a Changelog] a corresponding badge is shown. To encourage
  this, the default behaviour is set to `true` if semantic versioning is also active on the project. Set `changelog`
  to `false` to disable it.


## What's NOT included in this module?

This module does NOT handle the following items, which you may want to provide on your own:

- [Labels](#labels)
- [Memberships](#memberships)
- [CI/CD Pipelines](#cicd-pipelines)
- [CI/CD Variables](#cicd-variables)
- [Access Tokens](#access-tokens)
- [Deploy Keys and Tokens](#deploy-keys-and-tokens)
- [Push Mirroring](#push-mirroring)
- [Kubernetes Clusters](#kubernetes-clusters)
- [Integration and Webhooks](#integrations-and-webhooks)
- [GitLab Pages](#gitlab-pages)


### Labels

This module does not create any project labels. We do recommend to manage labels on the group via [`gitlab_group_label`]
resources and not on individual projects. Use the [`gitlab_label`] resource for very project specific labels, though.


### Memberships

This module does not handle any group and user memberships to the project as we recommend disabling it on the project
via group settings and handle permissions on the group level via the [`gitlab_group_membership`] resource only.


### CI/CD Pipelines

This module does not handle any other further CI/CD pipeline configurations or settings except enabling/disabling the
pipelines feature as a whole (see [`pipelines_enabled`](#project)), nor register GitLab runner instances.

Use the [`gitlab_pipeline_schedule`] and [`gitlab_pipeline_schedule_variable`] resources to
create [pipeline schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).

Use the [`gitlab_pipeline_trigger`] resource to create external pipeline triggers if not configurable by pipeline
triggers in the `.gitlab-ci.yaml` file.

Use the [`gitlab_project_freeze_period`] resource to configure [deploy freezes](https://docs.gitlab.com/ee/user/project/releases/index.html#prevent-unintentional-releases-by-setting-a-deploy-freeze)


### CI/CD Variables

This module does not handle any CI/CD variables. Use the [`gitlab_project_variable`] resource to configure any CI/CD
variables for the project or the [`gitlab_group_variable`] resource to configure them group wide.


### Access Tokens

This module does not handle any project access tokens and currently there is no Terraform resource to manage them!


### Deploy Keys and Tokens

This module does not handle any deploy keys or deploy tokens. Use the [`gitlab_deploy_token`], [`gitlab_deploy_key`]
and [`gitlab_deploy_key_enable`] resources for it.


### Push Mirroring

This module does not configure push mirroring, although it handles [Pull Mirroring](#pull-mirroring) on project
creation.

Use the [`gitlab_project_mirror`] resource to configure [push mirroring](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_mirror).


### Kubernetes Clusters

This module does not handle any Kubernetes cluster integrations. Use the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/index.html)
to connect a project to one or multiple clusters.


### Integrations and Webhooks

This module does not configure any service integrations or webhooks. Use the available `gitlab_service_*`
or [`gitlab_project_hook`] resources to configure integrations.


### GitLab Pages

This module does not handle any other further GitLab pages configurations or settings except enabling/disabling the
GitLab Pages feature as a whole (see [`pages_enabled`](#project)) and it's access level
(see [`pages_access_level`](#project)).

Currently, there is no Terraform resource to manage GitLab pages settings and domains.


[Conventional Commits]: https://www.conventionalcommits.org/en/v1.0.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/

<!-- Provider resource links -->
[`gitlab_branch_protection`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection
[`gitlab_deploy_token`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_token
[`gitlab_deploy_key`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key
[`gitlab_deploy_key_enable`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key_enable
[`gitlab_group_label`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_label
[`gitlab_group_membership`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_membership
[`gitlab_group_variable`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_variable
[`gitlab_label`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/label
[`gitlab_pipeline_schedule`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule
[`gitlab_pipeline_schedule_variable`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule_variable
[`gitlab_pipeline_trigger`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_trigger
[`gitlab_project_approval_rule`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_approval_rule
[`gitlab_project_badge`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_badge
[`gitlab_project_freeze_period`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_freeze_period
[`gitlab_project_hook`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_hook
[`gitlab_project_mirror`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_mirror
[`gitlab_project_variable`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable
[`gitlab_tag_protection`]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/tag_protection
